Certificate Expiry Munin Plugin
===============================

Uses `openssl` to read locally installed certificates and work out how
long before they expire. See comments in code for where to adjust
certificate filtering so you only see the ones you are interested in.

Installation
------------

Put the plugin file `cert_expiry` somewhere sensible (eg
`/usr/local/share/munin/plugins/cert_expiry`) and `chmod +x`. Then
link from your munin plugins folder, eg

`sudo ln -s /usr/local/share/munin/plugins/cert_expiry /etc/munin/plugins/`

If your certs are readable by any user on the system (the default
case) then you don't have any other config to do, so restart the
munin-node:

`sudo service munin-node restart`

